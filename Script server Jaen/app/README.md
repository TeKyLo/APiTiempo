#Proyecto Nefele

Descripcion

## ¿Qué incluye?

El proyecto está desarrollado con Flask, virtualenv, usa pymongo

## ¿Cómo iniciar el proyecto?

El primer paso es crear un entorno virtual, para que el proyecto completo
quede aislado del resto del sistema:


    ➤ virtualenv venv --no-site-packages


Luego se tiene que ingresar en el entorno virtual e instalar las dependencias:


    ➤ . venv/bin/activate        # o si se usa fish: . venv/bin/activate.fish
    ➤ make instalar

## Tareas con make

El resto de los comandos se pueden iniciar como tareas make, si escribís
``make`` en consola deberá aparecer el siguiente listado:
