# -*- coding: utf-8 -*-
from flask import Flask 
from flask_pymongo import PyMongo
import sys
sys.path.insert(0, 'scheduler/')
sys.path.insert(0, 'views/')

app = Flask(__name__)

app.config.from_pyfile('config.py')


mongo = PyMongo(app)
mongo2 = PyMongo(app,config_prefix='MONGO2')

from scheduler import *
from views import *


if __name__ == '__main__':
    app.run()
