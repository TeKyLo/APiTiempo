# -*- coding: utf-8 -*-
from run import app
from flask import render_template, jsonify, redirect, url_for, request
import requests
import sys
from modelsAemet import saveAemet
from modelsAemetPrediccion import saveAemetPre
from modelsOpenweathermap import saveOpen
from modelsTiempo import saveTiempo
from modelsWunderground import saveWunder
from modelsAccuweather import saveAccu
sys.path.insert(0, 'models/')

@app.route('/')
def index():   
    return render_template('index.html')

@app.route('/weather_info', methods=['POST'])
def weather_info():
    city = request.form["weather"]
    city = requests.get('http://weathers.co/api.php?city=' + city)
    weather = city.json()
    # Get data
    _location = weather.get('data').get('location', 'Error')
    _temp = weather.get('data').get('temperature', 'Error')
    _skytext = weather.get('data').get('skytext', 'Error')
    _wind = weather.get('data').get('wind', 'Error')
    if _temp == 'Error':
        return redirect(url_for('error'))
    return render_template('weather.html',location=_location, temp=_temp, skytext=_skytext, wind=_wind)

@app.route('/get_tiempo',methods=['GET'])
def getTiempo():
    return jsonify(get_tiempo())

@app.route('/get_openweathermap',methods=['GET'])
def getOpenweathermap():
    return jsonify(get_openweathermap())

@app.route('/get_wunderground',methods=['GET'])
def getWunderground():
    return jsonify(get_wunderground())

@app.route('/get_aemet',methods=['GET'])
def getAemet():
    return jsonify(datos_aemet())

@app.route('/get_aemetPrediccion',methods=['GET'])
def getAemetPrediccion():
    return jsonify(datos_aemetPrediccion())

@app.route('/get_accuweather',methods=['GET'])
def getAccuweather():
    return jsonify(get_accuweather())

@app.route('/error')
def error():
    return render_template('error.html')
