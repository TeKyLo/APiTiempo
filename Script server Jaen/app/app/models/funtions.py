# -*- coding: utf-8 -*-
from decimal import *
def gradeToSymbol(grade):
	if grade>=0 and grade<=22:
		symbol = "N"
	if grade>=23 and grade<=67:
		symbol = "NE"	
	if grade>=68 and grade<=112:
		symbol = "E"	
	if grade>=113 and grade<=157:
		symbol = "SE"		
	if grade>=158 and grade<=202:
		symbol = "S"
	if grade>=203 and grade<=247:
		symbol = "SO"
	if grade>=248 and grade<=292:
		symbol = "O"
	if grade>=293 and grade<=337:
		symbol = "NO"
	if grade>=338:
		symbol = "N"
	return symbol

def geographic(symbol):
	if symbol == "SW":
		return "SO"
	if symbol == "W":
		return "O"
	if symbol == "NW":
		return "NO"
	if symbol == "N":
		return "N"
	if symbol == "NE":
		return "NE"	
	if symbol == "E":
		return "E"
	if symbol == "SE":
		return "SE"	
	if symbol == "S":
		return "S"
	if symbol == "ENE":
		return "NE"
	if symbol == "ESE":
		return "SE"
	if symbol == "NNE":
		return "NE"
	if symbol == "NNW":
		return "NE"
	if symbol == "SSE":
		return "SE"
	if symbol == "WNE":
		return "NO"
	if symbol == "WSW":
		return "SO"


def metresToKm(metres):
	return float(metres * 3.6)

def interpolate(x,x1,x2,y1,y2):
	#getcontext().prec = 2
	re = float(y1) + ((float(Decimal(x)-Decimal(x1)) / float(Decimal(x2)-Decimal(x1))) * (float(y2)-float(y1)))
	resultado = "{0:.2f}".format(re,2)
	return Decimal(resultado)

