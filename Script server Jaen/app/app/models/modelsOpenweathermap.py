# -*- coding: utf-8 -*-
from run import mongo,  mongo2
from run import app
from config import url_openweathermap
import requests
from datetime import datetime, date, timedelta
import time
import json
from funtions import gradeToSymbol, metresToKm, interpolate
from bson.json_util import dumps


def llamadaOpenweathermap():
    global resp_openweathermap
    resp_openweathermap = requests.get(url_openweathermap)
    if resp_openweathermap.status_code == 401:
        print "Fallo coleccion Openweathermap: ", time.ctime()
        #return redirect(url_for('error'))
    else:
        saveDBOpenweathermap()

def saveDBOpenweathermap():
    with app.app_context():    
        formatoDateHour = "%d-%m-%y %H"
        dateHour = datetime.today().strftime(formatoDateHour)

        formatoDate = "%d-%m-%Y"
        date = datetime.today().strftime(formatoDate)
        obj_dateDatetime = datetime.strptime(date,formatoDate)

        formatoHour = "%H"
        hour = datetime.today().strftime(formatoHour)        
        obj_hourDatetime = datetime.strptime(hour,formatoHour)
        
        db = mongo.db.openweathermap
        #db.insert({"date": datetime.datetime.utcnow()},get_openweathermap())
        db.insert({"_id": dateHour,"fecha":obj_dateDatetime,"hora": obj_hourDatetime,"obj":get_openweathermap()})
        print "Guardado coleccion Openweathermap en DB_DatosMeteorologicos: ", time.ctime()



def get_openweathermap():
    return resp_openweathermap.json()

def saveOpen():
    with app.app_context():
        db = mongo.db.openweathermap
        db2 = mongo2.db.openweathermap    

        humidity = []
        dirWinds = []
        velWinds = []
        rain = []
        temperature = []
        clouds = []
        pressure = []
        hours = []
 
        auxHumidy = []
        auxDirWinds = []
        auxVelWinds = []
        auxRain = []
        auxTemperature = []
        auxClouds = []
        auxPressure = []
        auxHours = []

        totalHumidity = []
        totalHumidity2 = []
        totalDirWinds = []
        totalDirWinds2 = []
        totalVelWinds = []
        totalVelWinds2 = []        
        totalRain = []
        totalRain2 = []
        totalTemperature = []
        totalTemperature2 = []
        totalClouds = []
        totalClouds2 = []
        totalPressure = []
        totalPressure2 = []
        totalHours = []
        totalHours2 = []

        endFor = 0

        lenght = 0
        cont1 = 0
        cont2 = 1

        obj = json.loads(dumps(db.find({}).sort("$natural",-1)))#obtengo el ultimo
        _id=obj[0]['_id']
    
        dateHour = datetime.strptime(_id, '%d-%m-%y %H')#Quitar :%M:%S

        formatoDate = "%d-%m-%Y"
        date = dateHour.strftime(formatoDate)
        obj_dateDatetime = datetime.strptime(date,formatoDate)

        formatoHour = "%H"
        hour = dateHour.strftime(formatoHour)        
        obj_hourDatetime = datetime.strptime(hour,formatoHour)

        #recojo fecha para calcular 48 horas y donde parar los dor
        primeraFecha = obj[0]['obj']['list'][0]['dt_txt']
        datetimePrimera = datetime.strptime(primeraFecha, '%Y-%m-%d %H:%M:%S')


        end_date = datetimePrimera + timedelta(hours = 48)
        twodays = end_date.strftime('%Y-%m-%d %H:%M:%S')  
        #Determinar len for
        for x in xrange(0,len(obj[0]['obj']['list'])):
            dt_txt = obj[0]['obj']['list'][x]['dt_txt']
            if dt_txt == twodays:
                endFor = x           

        for j in xrange(0,endFor+1):
            humidity.append(float(obj[0]['obj']['list'][j]['main']['humidity']))

            hoursActu = obj[0]['obj']['list'][j]['dt_txt']
            obj_hourstime = datetime.strptime(hoursActu, '%Y-%m-%d %H:%M:%S')
            hours.append(obj_hourstime)

            symbol = gradeToSymbol(obj[0]['obj']['list'][j]['wind']['deg'])
            dirWinds.append(symbol)

            km = metresToKm(obj[0]['obj']['list'][j]['wind']['speed'])
            velWinds.append(km)

            if len(obj[0]['obj']['list'][j]) > 7:
                if len(obj[0]['obj']['list'][j]['rain']) > 0:
                    rain.append(float(obj[0]['obj']['list'][j]['rain']['3h']))
                else:
                    rain.append(float(0))
            else:
                rain.append(float(0))

            temperature.append(obj[0]['obj']['list'][j]['main']['temp'])

            clouds.append(float(obj[0]['obj']['list'][j]['clouds']['all']))

            pressure.append(obj[0]['obj']['list'][j]['main']['pressure'])

        for j in xrange(0,len(humidity)*2-1)   : 
            if j >= 1 and j%2!=0:
                for x in xrange(0,len(humidity)):
                    auxHumidy.append(humidity[x])  
                    auxDirWinds.append(dirWinds[x]) 
                    auxVelWinds.append(velWinds[x]) 
                    auxRain.append(rain[x]) 
                    auxTemperature.append(temperature[x]) 
                    auxClouds.append(clouds[x]) 
                    auxPressure.append(pressure[x]) 

                for x in xrange(0,len(hours)):     
                    auxHours.append(hours[x].strftime("%H"))
                if auxHours[cont2] == "00":
                    auxHours[cont2] = "24"

                oneHours = hours[cont1] + timedelta(hours = 1)
                twoHours = hours[cont1] + timedelta(hours = 2)
                x = oneHours.strftime("%H")
                _x = twoHours.strftime("%H")

                yHumidity = interpolate(x,auxHours[cont1],auxHours[cont2],auxHumidy[cont1],auxHumidy[cont2])
                _yHumidity = interpolate(_x,auxHours[cont1],auxHours[cont2],auxHumidy[cont1],auxHumidy[cont2])

                ydirWinds = auxDirWinds[cont1]
                _ydirWinds = auxDirWinds[cont2]

                yvelWinds = interpolate(x,auxHours[cont1],auxHours[cont2],auxVelWinds[cont1],auxVelWinds[cont2])
                _yvelWinds = interpolate(_x,auxHours[cont1],auxHours[cont2],auxVelWinds[cont1],auxVelWinds[cont2])

                yrain = interpolate(x,auxHours[cont1],auxHours[cont2],auxRain[cont1],auxRain[cont2])
                _yrain = interpolate(_x,auxHours[cont1],auxHours[cont2],auxRain[cont1],auxRain[cont2])

                ytemperature = interpolate(x,auxHours[cont1],auxHours[cont2],auxTemperature[cont1],auxTemperature[cont2])
                _ytemperature = interpolate(_x,auxHours[cont1],auxHours[cont2],auxTemperature[cont1],auxTemperature[cont2])

                yclouds = interpolate(x,auxHours[cont1],auxHours[cont2],auxClouds[cont1],auxClouds[cont2])
                _yclouds = interpolate(_x,auxHours[cont1],auxHours[cont2],auxClouds[cont1],auxClouds[cont2])

                ypressure = interpolate(x,auxHours[cont1],auxHours[cont2],auxPressure[cont1],auxPressure[cont2])
                _ypressure = interpolate(_x,auxHours[cont1],auxHours[cont2],auxPressure[cont1],auxPressure[cont2])
               
                cont1 = cont1 + 1 
                cont2 = cont2 + 1

                auxHumidy = []
                auxDirWinds = []
                auxVelWinds = []
                auxRain = []
                auxTemperature = []
                auxClouds = []
                auxPressure = []
                auxHours = []

                totalHumidity.append(float(yHumidity))
                totalHumidity2.append(float(_yHumidity))

                totalDirWinds.append(ydirWinds)
                totalDirWinds2.append(_ydirWinds)

                totalVelWinds.append(float(yvelWinds))
                totalVelWinds2.append(float(_yvelWinds))  

                totalRain.append(float(_yrain))
                totalRain2.append(float(_yrain))

                totalTemperature.append(float(ytemperature))
                totalTemperature2.append(float(_ytemperature))

                totalClouds.append(float(yclouds))
                totalClouds2.append(float(_yclouds))

                totalPressure.append(float(_ypressure))
                totalPressure2.append(float(_ypressure))

                totalHours.append(oneHours)
                totalHours2.append(twoHours)
                
        cont = 0
        for x in xrange(0,len(totalHumidity)*2):
            if x%2 == 1:
                humidity.insert(x,totalHumidity[cont])
                dirWinds.insert(x,totalDirWinds[cont])
                velWinds.insert(x,totalVelWinds[cont])
                rain.insert(x,totalRain[cont])
                temperature.insert(x,totalTemperature[cont])
                clouds.insert(x,totalClouds[cont])
                pressure.insert(x,totalPressure[cont])
                hours.insert(x,totalHours[cont])
                cont = cont + 1

        cont = 0
        for x in xrange(2,len(humidity)+len(totalHumidity2),3):
            humidity.insert(x,totalHumidity2[cont])
            dirWinds.insert(x,totalDirWinds2[cont])
            velWinds.insert(x,totalVelWinds2[cont])
            rain.insert(x,totalRain2[cont])
            temperature.insert(x,totalTemperature2[cont])
            clouds.insert(x,totalClouds2[cont])
            pressure.insert(x,totalPressure2[cont])
            hours.insert(x,totalHours2[cont])
            cont = cont + 1

        #calculo la dos fechas para construir mis json
        firstHour = hours[0]
        firstDay =firstHour + timedelta(hours = 24)
        twoDay =firstHour + timedelta(hours = 48)
        for x in xrange(0,len(hours)):           
            if firstDay == hours[x]:
                lenght = x
            if twoDay == hours[x]:
                endFor = x
        
        
        db2.insert(
                    {
                    "_id":_id,
                    "fecha":obj_dateDatetime,
                    "hora":obj_hourDatetime,
                        
                                "horas":[
                                    {
                                        "hora":hours[0],
                                        "humedad":{
                                            "valor":humidity[0],
                                            "unidad":"%"
                                        },
                                        "dirViento":{
                                            "valor":dirWinds[0],
                                        },
                                        "velViento":{
                                            "valor":velWinds[0],
                                            "unidad":"km/h"
                                        },
                                        "lluvia":{
                                            "valor":rain[0],
                                            "unidad":"mm"
                                        },
                                        "temperatura":{
                                            "valor":temperature[0],
                                            "unidad":"°C"
                                        },
                                        "presion":{
                                            "valor":pressure[0],
                                            "unidad":"hPa"
                                        },
                                        "nubes":{
                                            "valor":clouds[0],
                                            "unidad":"%"
                                        }
                                    }
                                ]
                            }
                        
                )

        for i in xrange(0,1):
            for x in xrange(1,lenght+1):
                db2.update(
                        {"_id":_id},
                            { '$push': 
                                {"horas": {
                                    "hora":hours[x],
                                    "humedad":{
                                        "valor":humidity[x],
                                        "unidad":"%"
                                    },
                                    "dirViento":{
                                        "valor":dirWinds[x],
                                    },
                                    "velViento":{
                                        "valor":velWinds[x],
                                        "unidad":"km/h"
                                    },
                                    "lluvia":{
                                        "valor":rain[x],
                                        "unidad":"mm"
                                    }
                                    ,
                                    "temperatura":{
                                        "valor":temperature[x],
                                        "unidad":"°C"
                                    }
                                    ,
                                    "presion":{
                                        "valor":pressure[x],
                                        "unidad":"hPa"
                                    },
                                    "nubes":{
                                        "valor":clouds[x],
                                        "unidad":"%"
                                    }
                                }

                            } 
                        }
                    )
        for i in xrange(1,2):
            for x in xrange(lenght+1,endFor):#+1 para una hora mas
                db2.update(
                        {"_id":_id},
                            { '$push': 
                                {"horas": {
                                    "hora":hours[x],
                                    "humedad":{
                                        "valor":humidity[x],
                                        "unidad":"%"
                                    },
                                    "dirViento":{
                                        "valor":dirWinds[x],
                                    },
                                    "velViento":{
                                        "valor":velWinds[x],
                                        "unidad":"km/h"
                                    },
                                    "lluvia":{
                                        "valor":rain[x],
                                        "unidad":"mm"
                                    }
                                    ,
                                    "temperatura":{
                                        "valor":temperature[x],
                                        "unidad":"°C"
                                    }
                                    ,
                                    "presion":{
                                        "valor":pressure[x],
                                        "unidad":"hPa"
                                    },
                                    "nubes":{
                                        "valor":clouds[x],
                                        "unidad":"%"
                                    }
                                }
                            } 
                        }
                    )
            

        print "Guardado coleccion Openweathermap en Historico: ", time.ctime()        