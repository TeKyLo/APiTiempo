# -*- coding: utf-8 -*-
from run import mongo, mongo2
from run import app
from config import url_wunderground
import requests
from datetime import datetime, date, timedelta
import time
import json
from funtions import geographic
from bson.json_util import dumps

def llamadaWunderground():
    global resp_wunderground
    resp_wunderground = requests.get(url_wunderground)
    saveDBWunderground()

def saveDBWunderground():
    with app.app_context():

        formatoDateHour = "%d-%m-%y %H"
        dateHour = datetime.today().strftime(formatoDateHour)

        formatoDate = "%d-%m-%Y"
        date = datetime.today().strftime(formatoDate)
        obj_dateDatetime = datetime.strptime(date,formatoDate)

        formatoHour = "%H"
        hour = datetime.today().strftime(formatoHour)        
        obj_hourDatetime = datetime.strptime(hour,formatoHour)
        
        db = mongo.db.wunderground
        db.insert({"_id": dateHour,"fecha":obj_dateDatetime,"hora": obj_hourDatetime,"obj":get_wunderground()})
        print "Guardado coleccion Wunderground en DB_DatosMeteorologicos: ", time.ctime()
       


def get_wunderground():
    return resp_wunderground.json()

def saveWunder():
    with app.app_context():
        db = mongo.db.wunderground
        db2 = mongo2.db.wunderground

        humidity = []
        dirWinds = []
        velWinds = []
        rain = []
        temperature = []
        clouds = []
        pressure = []
        hours = []

        obj = json.loads(dumps(db.find({}).sort("$natural",-1)))#obtengo el ultimo
        _id=obj[0]['_id']


        dateHour = datetime.strptime(_id, '%d-%m-%y %H')#Quitar :%M:%S

        formatoDate = "%d-%m-%Y"
        date = dateHour.strftime(formatoDate)
        obj_dateDatetime = datetime.strptime(date,formatoDate)


        formatoHour = "%H"
        hour = dateHour.strftime(formatoHour)        
        obj_hourDatetime = datetime.strptime(hour,formatoHour)


        for j in xrange(0,48):
            humidity.append(float(obj[0]['obj']['hourly_forecast'][j]['humidity']))

            auxHora = obj[0]['obj']['hourly_forecast'][j]['FCTTIME']['hour']
            auxDia = obj[0]['obj']['hourly_forecast'][j]['FCTTIME']['mday']
            auxMes = obj[0]['obj']['hourly_forecast'][j]['FCTTIME']['mon']
            auxAno = obj[0]['obj']['hourly_forecast'][j]['FCTTIME']['year']
            aux = auxAno+auxMes+auxDia+auxHora+"0000"
            
            obj_hourstime = datetime.strptime(aux, '%Y%m%d%H%M%S')
            hours.append(obj_hourstime)
            
           
            dirWinds.append(geographic(obj[0]['obj']['hourly_forecast'][j]['wdir']['dir']))

            velWinds.append(float(obj[0]['obj']['hourly_forecast'][j]['wspd']['metric']))
            rain.append(float(obj[0]['obj']['hourly_forecast'][j]['qpf']['metric']))
            temperature.append(float(obj[0]['obj']['hourly_forecast'][j]['temp']['metric']))
            clouds.append(float(obj[0]['obj']['hourly_forecast'][j]['sky']))
            pressure.append(float(obj[0]['obj']['hourly_forecast'][j]['mslp']['metric']))
        
        db2.insert(
                    {
                    "_id":_id,
                    "fecha":obj_dateDatetime,
                    "hora":obj_hourDatetime,
                        
                                "horas":[
                                    {
                                        "hora":hours[0],
                                        "humedad":{
                                            "valor":humidity[0],
                                            "unidad":"%"
                                        },
                                        "dirViento":{
                                            "valor":dirWinds[0],
                                        },
                                        "velViento":{
                                            "valor":velWinds[0],
                                            "unidad":"km/h"
                                        },
                                        "lluvia":{
                                            "valor":rain[0],
                                            "unidad":"mm"
                                        },
                                        "temperatura":{
                                            "valor":temperature[0],
                                            "unidad":"°C"
                                        },
                                        "presion":{
                                            "valor":pressure[0],
                                            "unidad":"hPa"
                                        },
                                        "nubes":{
                                            "valor":clouds[0],
                                            "unidad":"%"
                                        }
                                    }
                                ]
                            }
                        
                )

        for i in xrange(0,1):
            for x in xrange(1,48):
                db2.update(
                        {"_id":_id},
                            { '$push': 
                                {"horas": {
                                    "hora":hours[x],
                                    "humedad":{
                                        "valor":humidity[x],
                                        "unidad":"%"
                                    },
                                    "dirViento":{
                                        "valor":dirWinds[x],
                                    },
                                    "velViento":{
                                        "valor":velWinds[x],
                                        "unidad":"km/h"
                                    },
                                    "lluvia":{
                                        "valor":rain[x],
                                        "unidad":"mm"
                                    },
                                    "temperatura":{
                                        "valor":temperature[x],
                                        "unidad":"°C"
                                    },
                                    "presion":{
                                        "valor":pressure[x],
                                        "unidad":"hPa"
                                    },
                                    "nubes":{
                                        "valor":clouds[x],
                                        "unidad":"%"
                                    }
                                }

                            } 
                        }
                    )
        
    
        print "Guardado coleccion Wunderground en Historico: ", time.ctime()
        
