# -*- coding: utf-8 -*-
from run import mongo, mongo2
from run import app
from config import url_aemet
import requests
from datetime import datetime, date
import time
import json
from funtions import gradeToSymbol
from bson.json_util import dumps
#from requests.packages.urllib3.exceptions import InsecureRequestWarning #Quitar en server jaen
#Desactivar warning https aemet
#requests.packages.urllib3.disable_warnings(InsecureRequestWarning) #Quitar en server jaen

def llamadaAemet():
    global resp_aemet
    resp_aemet = requests.get(url_aemet, verify=False)
    if resp_aemet.status_code == 401:
        print "Fallo coleccion Aemet: ", time.ctime()
    else:
        saveDBAemet()

def saveDBAemet():
    with app.app_context():
        formatoDateHour = "%d-%m-%y %H"
        dateHour = datetime.today().strftime(formatoDateHour)

        formatoDate = "%d-%m-%Y"
        date = datetime.today().strftime(formatoDate)
        obj_dateDatetime = datetime.strptime(date,formatoDate)

        formatoHour = "%H"
        hour = datetime.today().strftime(formatoHour)        
        obj_hourDatetime = datetime.strptime(hour,formatoHour)

        db = mongo.db.aemet
        db.insert({"_id": dateHour,"fecha":obj_dateDatetime,"hora": obj_hourDatetime,"obj":datos_aemet()})
        print "Guardado coleccion Aemet en DB_DatosMeteorologicos: ", time.ctime()


def get_aemet():
    return resp_aemet.json()

def datos_aemet():
    json_object = get_aemet()
    datos = (json_object['datos'])#recojo el campo que me interesa
    resp_aemet = requests.get(datos, verify=False)#vuelvo hacer la llamada
    return resp_aemet.json()

def saveAemet():
    with app.app_context():
        db = mongo.db.aemet
        db2 = mongo2.db.aemet

        humidity = []
        dirWinds = []
        velWinds = []
        rain = []
        temperature = []
        clouds = []
        pressure = []
        hours = []

        obj = json.loads(dumps(db.find({}).sort("$natural",-1)))#obtengo el ultimo
        _id=obj[0]['_id']

        dateHour = datetime.strptime(_id, '%d-%m-%y %H')#Quitar :%M:%S

        formatoDate = "%d-%m-%Y"
        date = dateHour.strftime(formatoDate)
        obj_dateDatetime = datetime.strptime(date,formatoDate)


        formatoHour = "%H"
        hour = dateHour.strftime(formatoHour)        
        obj_hourDatetime = datetime.strptime(hour,formatoHour)

        for j in xrange(0,len(obj[0]['obj'])):
            humidity.append(obj[0]['obj'][j]['hr'])

            aux = obj[0]['obj'][j]['fint']
            obj_hourstime = datetime.strptime(aux, '%Y-%m-%dT%H:%M:%S')
            hours.append(obj_hourstime)
           
            symbol = gradeToSymbol(obj[0]['obj'][j]['dv'])
            dirWinds.append(symbol)

            velWinds.append(obj[0]['obj'][j]['vv'])
            rain.append(obj[0]['obj'][j]['prec'])
            temperature.append(obj[0]['obj'][j]['ta'])
            clouds.append(None)
            pressure.append(obj[0]['obj'][j]['pres'])
        
        db2.insert(
                    {
                    "_id":_id,
                    "fecha":obj_dateDatetime,
                    "hora":obj_hourDatetime,
                        
                                "horas":[
                                    {
                                        "hora":hours[0],
                                        "humedad":{
                                            "valor":humidity[0],
                                            "unidad":"%"
                                        },
                                        "dirViento":{
                                            "valor":dirWinds[0],
                                        },
                                        "velViento":{
                                            "valor":velWinds[0],
                                            "unidad":"km/h"
                                        },
                                        "lluvia":{
                                            "valor":rain[0],
                                            "unidad":"mm"
                                        },
                                        "temperatura":{
                                            "valor":temperature[0],
                                            "unidad":"°C"
                                        },
                                        "presion":{
                                            "valor":pressure[0],
                                            "unidad":"hPa"
                                        },
                                        "nubes":{
                                            "valor":clouds[0],
                                            "unidad":"%"
                                        }
                                    }
                                ]
                            }
                        
                )

        for i in xrange(0,1):
            for x in xrange(1,len(obj[0]['obj'])):
                db2.update(
                        {"_id":_id},
                            { '$push': 
                                {"horas": {
                                    "hora":hours[x],
                                    "humedad":{
                                        "valor":humidity[x],
                                        "unidad":"%"
                                    },
                                    "dirViento":{
                                        "valor":dirWinds[x],
                                    },
                                    "velViento":{
                                        "valor":velWinds[x],
                                        "unidad":"km/h"
                                    },
                                    "lluvia":{
                                        "valor":rain[x],
                                        "unidad":"mm"
                                    },
                                    "temperatura":{
                                        "valor":temperature[x],
                                        "unidad":"°C"
                                    },
                                    "presion":{
                                        "valor":pressure[x],
                                        "unidad":"hPa"
                                    },
                                    "nubes":{
                                        "valor":clouds[x],
                                        "unidad":"%"
                                    }
                                }

                            } 
                        }
                    )
        
        
        print "Guardado coleccion Aemet en Historico: ", time.ctime()