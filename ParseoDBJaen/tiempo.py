# -*- coding: utf-8 -*-

import pymongo
import pprint
from flask import Flask 
from flask_pymongo import PyMongo
import json
from datetime import datetime, date, timedelta


app = Flask(__name__)


app.config['MONGO_DBNAME'] = 'Historico'#DB_DatosMeteorologicos#Historico
app.config['MONGO_URI'] = 'mongodb://localhost:27017/Historico'

app.config['MONGO2_DBNAME'] = 'prue'
app.config['MONGO2_URI'] = 'mongodb://localhost:27017/prue'




mongo = PyMongo(app)
with app.app_context():
	db = mongo.db.tiempo
	#pprint.pprint(db.aemet.count())
	mongo2 = PyMongo(app,config_prefix='MONGO2')

def geographic(symbol):
	if symbol == "SW":
		return "SO"
	if symbol == "W":
		return "O"
	if symbol == "NW":
		return "NO"
	if symbol == "N":
		return "N"
	if symbol == "NE":
		return "NE"	
	if symbol == "E":
		return "E"
	if symbol == "SE":
		return "SE"	
	if symbol == "S":
		return "S"

def saveAemet():
	with app.app_context():


	    db2 = mongo2.db.tiempo
	    #db = mongo.db.aemet

	    from bson.json_util import dumps
	    #Codigo para server Jaen
	    humidity = []
	    dirWinds = []
	    velWinds = []
	    rain = []
	    temperature = []
	    pressure = []
	    clouds = []
	    hours = []



        obj = json.loads(dumps(db.find({})))#obtengo el ultimo
        for i in xrange(0,db.count()):
	        _id=obj[i]['_id']
	        flag = 0
	        if _id == "24-04-17 18:00:00" or _id == "25-04-17 00:00:00" or _id == "25-04-17 06:00:00":
	        	_id = _id.replace(":00:00","")

	        dateHour = datetime.strptime(_id, '%d-%m-%y %H:%M:%S')

	        formatoDate = "%d-%m-%Y"
	        date = dateHour.strftime(formatoDate)
	        obj_dateDatetime = datetime.strptime(date,formatoDate)

	        formatoHour = "%H"
	        hour = dateHour.strftime(formatoHour)        
	        obj_hourDatetime = datetime.strptime(hour,formatoHour)
	        	

	        for h in xrange(0,len(obj[i]['obj']['report']['location']['day'])):
	        	aux = obj[i]['obj']['report']['location']['day'][h]['value']
	        	obj_hourstime = datetime.strptime(aux, '%Y%m%d')
	        	for j in xrange(0,len(obj[i]['obj']['report']['location']['day'][h]['hour'])):
	        		humidity.append(float(obj[i]['obj']['report']['location']['day'][h]['hour'][j]['humidity']['value']))
	        		periodo = obj[i]['obj']['report']['location']['day'][h]['hour'][j]['value']
	        		periodo = periodo.replace(":00","")
	        		end_date = obj_hourstime + timedelta(hours=int(periodo))

	        		auxClouds = obj[i]['obj']['report']['location']['day'][h]['hour'][j]['clouds']['value']
	        		auxClouds = auxClouds.replace("%","")
	        		clouds.append(float(auxClouds))
	        		

	        		hours.append(end_date)
	        		dirWinds.append(geographic(obj[i]['obj']['report']['location']['day'][h]['hour'][j]['wind']['dir']))
	        		velWinds.append(float(obj[i]['obj']['report']['location']['day'][h]['hour'][j]['wind']['value']))
	        		rain.append(float(obj[i]['obj']['report']['location']['day'][h]['hour'][j]['rain']['value']))
	        		temperature.append(float(obj[i]['obj']['report']['location']['day'][h]['hour'][j]['temp']['value']))
	        		pressure.append(float(obj[i]['obj']['report']['location']['day'][h]['hour'][j]['pressure']['value']))

                if flag == 0:
	                flag = 1
	                db2.insert(
	                        {
	                        "_id":_id,
	                        "fecha":obj_dateDatetime,
	                        "hora":obj_hourDatetime,
	                            
	                                
	                                    "horas":[
	                                        {
	                                            "hora":hours[0],
	                                            "humedad":{
	                                                "valor":humidity[0],
	                                                "unidad":"%"
	                                            },
	                                            "dirViento":{
	                                                "valor":dirWinds[0],
	                                            },
	                                            "velViento":{
	                                                "valor":velWinds[0],
	                                                "unidad":"km/h"
	                                            },
	                                            "lluvia":{
	                                                "valor":rain[0],
	                                                "unidad":"mm"
	                                            }
	                                            ,
	                                            "temperatura":{
	                                                "valor":temperature[0],
	                                                "unidad":"°C"
	                                            }
	                                            ,
	                                            "presion":{
	                                                "valor":pressure[0],
	                                                "unidad":"hPa"
	                                            },
                                            "nubes":{
                                                "valor":clouds[0],
                                                "unidad":"%"
                                            }
	                                        }
	                                    ]
	                                
	                                                                                       
	                        }
	                    )

	                for j in xrange(0,1):
	                    for x in xrange(1,len(obj[0]['obj']['report']['location']['day'][j]['hour'])):
	                        db2.update(
	                                {"_id":_id},
	                                    { '$push': 
	                                        {"horas": {
	                                            "hora":hours[x],
	                                            "humedad":{
	                                                "valor":humidity[x],
	                                                "unidad":"%"
	                                            },
	                                            "dirViento":{
	                                                "valor":dirWinds[x],
	                                            },
	                                            "velViento":{
	                                                "valor":velWinds[x],
	                                                "unidad":"km/h"
	                                            },
	                                            "lluvia":{
	                                                "valor":rain[x],
	                                                "unidad":"mm"
	                                            }
	                                            ,
	                                            "temperatura":{
	                                                "valor":temperature[x],
	                                                "unidad":"°C"
	                                            }
	                                            ,
	                                            "presion":{
	                                                "valor":pressure[x],
	                                                "unidad":"hPa"
	                                            },
                                            "nubes":{
                                                "valor":clouds[x],
                                                "unidad":"%"
                                            }
	                                        }

	                                    } 
	                                }
	                            )
	           	if j == 1:
	           		flag = 2
	                for j in xrange(1,2):
	                    for x in xrange(0,len(obj[0]['obj']['report']['location']['day'][j]['hour'])):
	                        db2.update(
	                                {"_id":_id},
	                                    { '$push': 
	                                        {"horas": {
	                                            "hora":hours[x],
	                                            "humedad":{
	                                                "valor":humidity[x],
	                                                "unidad":"%"
	                                            },
	                                            "dirViento":{
	                                                "valor":dirWinds[x],
	                                            },
	                                            "velViento":{
	                                                "valor":velWinds[x],
	                                                "unidad":"km/h"
	                                            },
	                                            "lluvia":{
	                                                "valor":rain[x],
	                                                "unidad":"mm"
	                                            }
	                                            ,
	                                            "temperatura":{
	                                                "valor":temperature[x],
	                                                "unidad":"°C"
	                                            }
	                                            ,
	                                            "presion":{
	                                                "valor":pressure[x],
	                                                "unidad":"hPa"
	                                            },
                                            "nubes":{
                                                "valor":clouds[x],
                                                "unidad":"%"
                                            }
	                                        }

	                                    } 
	                                }
	                            )

				if j == 2:
					for j in xrange(2,3):
						for x in xrange(0,len(obj[0]['obj']['report']['location']['day'][j]['hour'])):
							db2.update(
	                                {"_id":_id},
	                                    { '$push': 
	                                        {"horas": {
	                                            "hora":hours[x],
	                                            "humedad":{
	                                                "valor":humidity[x],
	                                                "unidad":"%"
	                                            },
	                                            "dirViento":{
	                                                "valor":dirWinds[x],
	                                            },
	                                            "velViento":{
	                                                "valor":velWinds[x],
	                                                "unidad":"km/h"
	                                            },
	                                            "lluvia":{
	                                                "valor":rain[x],
	                                                "unidad":"mm"
	                                            }
	                                            ,
	                                            "temperatura":{
	                                                "valor":temperature[x],
	                                                "unidad":"°C"
	                                            }
	                                            ,
	                                            "presion":{
	                                                "valor":pressure[x],
	                                                "unidad":"hPa"
	                                            },
                                            "nubes":{
                                                "valor":clouds[x],
                                                "unidad":"%"
                                            }
	                                        }

	                                    } 
	                                }
	                            )
		        humidity = []
		        dirWinds = []
	           	velWinds = []
	           	rain = []
	           	temperature = []
	           	pressure = []
	           	clouds = []
	           	hours = []		            
saveAemet()



