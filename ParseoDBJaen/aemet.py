# -*- coding: utf-8 -*-

import pymongo
import pprint
from flask import Flask 
from flask_pymongo import PyMongo
import json
from datetime import datetime, date


app = Flask(__name__)


app.config['MONGO_DBNAME'] = 'DB_DatosMeteorologicos'#DB_DatosMeteorologicos#Historico
app.config['MONGO_URI'] = 'mongodb://localhost:27017/DB_DatosMeteorologicos'

app.config['MONGO2_DBNAME'] = 'Historico'
app.config['MONGO2_URI'] = 'mongodb://localhost:27017/Historico'




mongo = PyMongo(app)
with app.app_context():
	db = mongo.db.aemet
	#pprint.pprint(db.aemet.count())
	mongo2 = PyMongo(app,config_prefix='MONGO2')

def gradeToSymbol(grade):
	if grade>=0 and grade<=22:
		symbol = "N"
	if grade>=23 and grade<=67:
		symbol = "NE"	
	if grade>=68 and grade<=112:
		symbol = "E"	
	if grade>=113 and grade<=157:
		symbol = "SE"		
	if grade>=158 and grade<=202:
		symbol = "S"
	if grade>=203 and grade<=247:
		symbol = "SO"
	if grade>=248 and grade<=292:
		symbol = "O"
	if grade>=293 and grade<=337:
		symbol = "NO"
	if grade>=338:
		symbol = "N"


def saveAemet():
	with app.app_context():


	    db2 = mongo2.db.aemet
	    #db = mongo.db.aemet

	    from bson.json_util import dumps
	    #Codigo para server Jaen
	    humidity = []
	    dirWinds = []
	    velWinds = []
	    rain = []
	    temperature = []
	    pressure = []
	    clouds = []
	    hours = []

	    obj = json.loads(dumps(db.find({})))#tengo la lista de colecciones

	    for i in xrange(0,db.count()):
	        _id=obj[i]['_id']

	        if _id == "24-04-17 18:00:00" or _id == "25-04-17 00:00:00" or _id == "25-04-17 06:00:00":
	            _id = _id.replace(":00:00","")

	        #dateHour = datetime.strptime(_id, '%d-%m-%y %H')
	        dateHour = datetime.strptime(_id, '%d-%m-%y %H:%M:%S')
	        #print dateHour

	        formatoDate = "%d-%m-%Y"
	        date = dateHour.strftime(formatoDate)
	        obj_dateDatetime = datetime.strptime(date,formatoDate)
	        #print obj_dateDatetime

	        formatoHour = "%H"
	        hour = dateHour.strftime(formatoHour)        
	        obj_hourDatetime = datetime.strptime(hour,formatoHour)

	        for j in xrange(0,len(obj[i]['obj'])):
	            humidity.append(obj[i]['obj'][j]['hr'])

	            aux = obj[i]['obj'][j]['fint']
	            obj_hourstime = datetime.strptime(aux, '%Y-%m-%dT%H:%M:%S')
	            hours.append(obj_hourstime)
	           
	            symbol = gradeToSymbol(obj[i]['obj'][j]['dv'])
	            dirWinds.append(symbol)

	            velWinds.append(obj[i]['obj'][j]['vv'])
	            rain.append(obj[i]['obj'][j]['prec'])
	            temperature.append(obj[i]['obj'][j]['ta'])
	            clouds.append(None)
	            pressure.append(obj[i]['obj'][j]['pres'])
	        
	        db2.insert(
                    {
                    "_id":_id,
                    "fecha":obj_dateDatetime,
                    "hora":obj_hourDatetime,
                        
                                "horas":[
                                    {
                                        "hora":hours[0],
                                        "humedad":{
                                            "valor":humidity[0],
                                            "unidad":"%"
                                        },
                                        "dirViento":{
                                            "valor":dirWinds[0],
                                        },
                                        "velViento":{
                                            "valor":velWinds[0],
                                            "unidad":"km/h"
                                        },
                                        "lluvia":{
                                            "valor":rain[0],
                                            "unidad":"mm"
                                        },
                                        "temperatura":{
                                            "valor":temperature[0],
                                            "unidad":"°C"
                                        },
                                        "presion":{
                                            "valor":pressure[0],
                                            "unidad":"hPa"
                                        },
                                        "nubes":{
                                            "valor":clouds[0],
                                            "unidad":"%"
                                        }
                                    }
                                ]
                            }
                        
                )
	        for i in xrange(0,1):
	        	for x in xrange(1,len(obj[0]['obj'])):
	        		db2.update(
                        {"_id":_id},
                            { '$push': 
                                {"horas": {
                                    "hora":hours[x],
                                    "humedad":{
                                        "valor":humidity[x],
                                        "unidad":"%"
                                    },
                                    "dirViento":{
                                        "valor":dirWinds[x],
                                    },
                                    "velViento":{
                                        "valor":velWinds[x],
                                        "unidad":"km/h"
                                    },
                                    "lluvia":{
                                        "valor":rain[x],
                                        "unidad":"mm"
                                    },
                                    "temperatura":{
                                        "valor":temperature[x],
                                        "unidad":"°C"
                                    },
                                    "presion":{
                                        "valor":pressure[x],
                                        "unidad":"hPa"
                                    },
                                    "nubes":{
                                        "valor":clouds[x],
                                        "unidad":"%"
                                    }
                                }

                            } 
                        }
                    )

	        humidity = []
	        dirWinds = []
	        velWinds = []
	        rain = []
	        hours = []
	        temperature = []
	        pressure = []
	        clouds = []
	        
	        


saveAemet()
