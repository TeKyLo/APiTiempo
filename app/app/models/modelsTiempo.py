# -*- coding: utf-8 -*-
from run import mongo, mongo2
from run import app
from config import url_tiempo
import requests
from datetime import datetime, date, timedelta
import time
import json
import xml.etree.ElementTree as ET
from collections import defaultdict
from funtions import geographic
from bson.json_util import dumps

def llamadaTiempo():
    global resp_tiempo
    resp_tiempo = requests.get(url_tiempo)
    if resp_tiempo.headers['content-type'] == "text/html; charset=UTF-8":
        print "Fallo coleccion Tiempo:", time.ctime()
    else:
        saveDBTiempo()

def saveDBTiempo():
    with app.app_context():
        formatoDateHour = "%d-%m-%y %H"
        dateHour = datetime.today().strftime(formatoDateHour)

        formatoDate = "%d-%m-%Y"
        date = datetime.today().strftime(formatoDate)
        obj_dateDatetime = datetime.strptime(date,formatoDate)

        formatoHour = "%H"
        hour = datetime.today().strftime(formatoHour)        
        obj_hourDatetime = datetime.strptime(hour,formatoHour)
        
        db = mongo.db.tiempo
        #db.find({"date":"2017-03-31 08:55:01.747Z"})
        db.insert({"_id": dateHour,"fecha":obj_dateDatetime,"hora": obj_hourDatetime,"obj":get_tiempo()})
        print "Guardado coleccion Tiempo en DB_DatosMeteorologicos: ", time.ctime()
        



def get_tiempo():
    return parseoJson(resp_tiempo)

def parseoJson(obj):
    tree = ET.XML(resp_tiempo.content)
    json_tiempo = etree_to_dict(tree)
    return json_tiempo

def etree_to_dict(t):
    d = {t.tag: {} if t.attrib else None}
    children = list(t)
    if children:
        dd = defaultdict(list)
        for dc in map(etree_to_dict, children):
            for k, v in dc.iteritems():
                dd[k].append(v)
        d = {t.tag: {k:v[0] if len(v) == 1 else v for k, v in dd.iteritems()}}
    if t.attrib:
        d[t.tag].update((k, v) for k, v in t.attrib.iteritems())
    if t.text:
        text = t.text.strip()
        if children or t.attrib:
            if text:
              d[t.tag]['text'] = text
        else:
            d[t.tag] = text
    return d

def saveTiempo():
    with app.app_context():
        db = mongo.db.tiempo
        db2 = mongo2.db.tiempo

        humidity = []
        dirWinds = []
        velWinds = []
        rain = []
        temperature = []
        pressure = []
        clouds = []
        hours = []

        flag = 0

        obj = json.loads(dumps(db.find({}).sort("_id",-1)))#obtengo el ultimo
        _id=obj[0]['_id']



        dateHour = datetime.strptime(_id, '%d-%m-%y %H')#Quitar :%M:%S

        formatoDate = "%d-%m-%Y"
        date = dateHour.strftime(formatoDate)
        obj_dateDatetime = datetime.strptime(date,formatoDate)

        formatoHour = "%H"
        hour = dateHour.strftime(formatoHour)        
        obj_hourDatetime = datetime.strptime(hour,formatoHour)

        for i in xrange(0,len(obj[0]['obj']['report']['location']['day'])):
            aux = obj[0]['obj']['report']['location']['day'][i]['value']
            obj_hourstime = datetime.strptime(aux, '%Y%m%d')

        
            for j in xrange(0,len(obj[0]['obj']['report']['location']['day'][i]['hour'])):
                humidity.append(float(obj[0]['obj']['report']['location']['day'][i]['hour'][j]['humidity']['value']))
                periodo = obj[0]['obj']['report']['location']['day'][i]['hour'][j]['value']
                periodo = periodo.replace(":00","")
                end_date = obj_hourstime + timedelta(hours=int(periodo))
                hours.append(end_date)


                dirWinds.append(geographic(obj[0]['obj']['report']['location']['day'][i]['hour'][j]['wind']['dir']))
                velWinds.append(float(obj[0]['obj']['report']['location']['day'][i]['hour'][j]['wind']['value']))
                rain.append(float(obj[0]['obj']['report']['location']['day'][i]['hour'][j]['rain']['value']))
                temperature.append(float(obj[0]['obj']['report']['location']['day'][i]['hour'][j]['temp']['value']))
                pressure.append(float(obj[0]['obj']['report']['location']['day'][i]['hour'][j]['pressure']['value']))
                auxClouds = obj[0]['obj']['report']['location']['day'][i]['hour'][j]['clouds']['value']
                auxClouds = auxClouds.replace("%","")                
                clouds.append(float(auxClouds))

            if flag == 0:
                flag = 1
                db2.insert(
                        {
                        "_id":_id,
                        "fecha":obj_dateDatetime,
                        "hora":obj_hourDatetime,

                                    "horas":[
                                        {
                                            "hora":hours[0],
                                            "humedad":{
                                                "valor":humidity[0],
                                                "unidad":"%"
                                            },
                                            "dirViento":{
                                                "valor":dirWinds[0],
                                            },
                                            "velViento":{
                                                "valor":velWinds[0],
                                                "unidad":"km/h"
                                            },
                                            "lluvia":{
                                                "valor":rain[0],
                                                "unidad":"mm"
                                            },
                                            "temperatura":{
                                                "valor":temperature[0],
                                                "unidad":"°C"
                                            },
                                            "presion":{
                                                "valor":pressure[0],
                                                "unidad":"hPa"
                                            },
                                            "nubes":{
                                                "valor":clouds[0],
                                                "unidad":"%"
                                            }
                                        }
                                    ]
                                }
                            
                    )
                
                for i in xrange(0,1):
                    for x in xrange(1,len(obj[0]['obj']['report']['location']['day'][i]['hour'])):
                        db2.update(
                                {"_id":_id},
                                    { '$push': 
                                        {"horas": {
                                            "hora":hours[x],
                                            "humedad":{
                                                "valor":humidity[x],
                                                "unidad":"%"
                                            },
                                            "dirViento":{
                                                "valor":dirWinds[x],
                                            },
                                            "velViento":{
                                                "valor":velWinds[x],
                                                "unidad":"km/h"
                                            },
                                            "lluvia":{
                                                "valor":rain[x],
                                                "unidad":"mm"
                                            },
                                            "temperatura":{
                                                "valor":temperature[x],
                                                "unidad":"°C"
                                            },
                                            "presion":{
                                                "valor":pressure[x],
                                                "unidad":"hPa"
                                            },
                                            "nubes":{
                                                "valor":clouds[x],
                                                "unidad":"%"
                                            }
                                        }

                                    } 
                                }
                            )
            if i == 1:
                for i in xrange(1,2):
                    for x in xrange(0,len(obj[0]['obj']['report']['location']['day'][i]['hour'])):
                        db2.update(
                                {"_id":_id},
                                    { '$push': 
                                        {"horas": {
                                            "hora":hours[x],
                                            "humedad":{
                                                "valor":humidity[x],
                                                "unidad":"%"
                                            },
                                            "dirViento":{
                                                "valor":dirWinds[x],
                                            },
                                            "velViento":{
                                                "valor":velWinds[x],
                                                "unidad":"km/h"
                                            },
                                            "lluvia":{
                                                "valor":rain[x],
                                                "unidad":"mm"
                                            },
                                            "temperatura":{
                                                "valor":temperature[x],
                                                "unidad":"°C"
                                            },
                                            "presion":{
                                                "valor":pressure[x],
                                                "unidad":"hPa"
                                            },
                                            "nubes":{
                                                "valor":clouds[x],
                                                "unidad":"%"
                                            }
                                        }

                                    } 
                                }
                            )
            humidity = []
            dirWinds = []
            velWinds = []
            rain = []
            temperature = []
            pressure = []
            clouds = []
            hours = []


        print "Guardado coleccion Tiempo en Historico: ", time.ctime()            
                



