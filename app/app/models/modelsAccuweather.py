# -*- coding: utf-8 -*-
from run import mongo, mongo2
from run import app
from config import url_accuweather
import requests
from datetime import datetime, date, timedelta
import time
import json
from funtions import gradeToSymbol
from bson.json_util import dumps

def llamadaAccuweather():
    global resp_accuweather
    resp_accuweather = requests.get(url_accuweather)
    if resp_accuweather.status_code == 401:
        print "Fallo coleccion Accuweather: ", time.ctime()
    else:
        saveDBAccuweather()


def saveDBAccuweather():
    with app.app_context():
        formatoDateHour = "%d-%m-%y %H"
        dateHour = datetime.today().strftime(formatoDateHour)

        formatoDate = "%d-%m-%Y"
        date = datetime.today().strftime(formatoDate)
        obj_dateDatetime = datetime.strptime(date,formatoDate)

        formatoHour = "%H"
        hour = datetime.today().strftime(formatoHour)        
        obj_hourDatetime = datetime.strptime(hour,formatoHour) 
          
        db = mongo.db.accuweather
        db.insert({"_id": dateHour,"fecha":obj_dateDatetime,"hora": obj_hourDatetime,"obj":get_accuweather()})
        print "Guardado coleccion Accuweather en DB_DatosMeteorologicos: ", time.ctime()



def get_accuweather():
    return resp_accuweather.json()


def saveAccu():
    with app.app_context():
        db = mongo.db.accuweather
        db2 = mongo2.db.accuweather
     
        humidity = []
        dirWinds = []
        velWinds = []
        rain = []
        temperature = []
        clouds = []
        pressure = []
        hours = []

        obj = json.loads(dumps(db.find({}).sort("_id",-1)))#obtengo el ultimo
        _id=obj[0]['_id']

        dateHour = datetime.strptime(_id, '%d-%m-%y %H')#Quitar :%M:%S
        formatoDate = "%d-%m-%Y"
        date = dateHour.strftime(formatoDate)
        obj_dateDatetime = datetime.strptime(date,formatoDate)


        formatoHour = "%H"
        hour = dateHour.strftime(formatoHour)        
        obj_hourDatetime = datetime.strptime(hour,formatoHour)


        for j in xrange(0,len(obj[0]['obj'])):
            humidity.append(float(obj[0]['obj'][j]['RelativeHumidity']))

            aux = obj[0]['obj'][j]['DateTime']
            aux = aux.rstrip("02:00")
            obj_hourstime = datetime.strptime(aux, '%Y-%m-%dT%H:%M:%S+')
            hours.append(obj_hourstime)
           
            dirWinds.append(gradeToSymbol(obj[0]['obj'][j]['Wind']['Direction']['Degrees']))

            velWinds.append(obj[0]['obj'][j]['Wind']['Speed']['Value'])
            rain.append(obj[0]['obj'][j]['Rain']['Value'])
            temperature.append(obj[0]['obj'][j]['Temperature']['Value'])
            clouds.append(float(obj[0]['obj'][j]['CloudCover']))
            pressure.append(None)
        
        db2.insert(
                    {
                    "_id":_id,
                    "fecha":obj_dateDatetime,
                    "hora":obj_hourDatetime,
                        
                                "horas":[
                                    {
                                        "hora":hours[0],
                                        "humedad":{
                                            "valor":humidity[0],
                                            "unidad":"%"
                                        },
                                        "dirViento":{
                                            "valor":dirWinds[0],
                                        },
                                        "velViento":{
                                            "valor":velWinds[0],
                                            "unidad":"km/h"
                                        },
                                        "lluvia":{
                                            "valor":rain[0],
                                            "unidad":"mm"
                                        },
                                        "temperatura":{
                                            "valor":temperature[0],
                                            "unidad":"°C"
                                        },
                                        "presion":{
                                            "valor":pressure[0],
                                            "unidad":"hPa"
                                        },
                                        "nubes":{
                                            "valor":clouds[0],
                                            "unidad":"%"
                                        }
                                    }
                                ]
                            }
                        
                )

        for i in xrange(0,1):
            for x in xrange(1,len(obj[0]['obj'])):
                db2.update(
                        {"_id":_id},
                            { '$push': 
                                {"horas": {
                                    "hora":hours[x],
                                    "humedad":{
                                        "valor":humidity[x],
                                        "unidad":"%"
                                    },
                                    "dirViento":{
                                        "valor":dirWinds[x],
                                    },
                                    "velViento":{
                                        "valor":velWinds[x],
                                        "unidad":"km/h"
                                    },
                                    "lluvia":{
                                        "valor":rain[x],
                                        "unidad":"mm"
                                    },
                                    "temperatura":{
                                        "valor":temperature[x],
                                        "unidad":"°C"
                                    },
                                    "presion":{
                                        "valor":pressure[x],
                                        "unidad":"hPa"
                                    },
                                    "nubes":{
                                        "valor":clouds[x],
                                        "unidad":"%"
                                    }
                                }

                            } 
                        }
                    )
        
    
        print "Guardado coleccion Accuweather en Historico: ", time.ctime()
        




