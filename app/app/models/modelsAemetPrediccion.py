# -*- coding: utf-8 -*-
from run import mongo, mongo2
from run import app
from config import url_aemetPrediccion
import requests
from datetime import datetime, date, timedelta
import time
import json
from bson.json_util import dumps
#from requests.packages.urllib3.exceptions import InsecureRequestWarning #Quitar en server jaen
#Desactivar warning https aemet
#requests.packages.urllib3.disable_warnings(InsecureRequestWarning) #Quitar en server jaen

def llamadaAemetPrediccion():
    global resp_aemetPrediccion
    resp_aemetPrediccion = requests.get(url_aemetPrediccion, verify=False)
    if resp_aemetPrediccion.status_code == 401:
        print "Fallo coleccion AemetPrediccion: ", time.ctime()
    else:
        saveDBAemetPrediccion()


def saveDBAemetPrediccion():
    with app.app_context():
        
        formatoDateHour = "%d-%m-%y %H"
        dateHour = datetime.today().strftime(formatoDateHour)

        formatoDate = "%d-%m-%Y"
        date = datetime.today().strftime(formatoDate)
        obj_dateDatetime = datetime.strptime(date,formatoDate)

        formatoHour = "%H"
        hour = datetime.today().strftime(formatoHour)        
        obj_hourDatetime = datetime.strptime(hour,formatoHour)
        
        db = mongo.db.aemetPrediccion
        db.insert({"_id": dateHour,"fecha":obj_dateDatetime,"hora": obj_hourDatetime,"obj":datos_aemetPrediccion()})
        print "Guardado coleccion AemetPrediccion en DB_DatosMeteorologicos: ", time.ctime()


def get_aemetPrediccion():
    return resp_aemetPrediccion.json()


def datos_aemetPrediccion():
    json_object = get_aemetPrediccion()
    datos = (json_object['datos'])#recojo el campo que me interesa
    resp_aemetPrediccion = requests.get(datos, verify=False)#vuelvo hacer la llamada
    return resp_aemetPrediccion.json()


def saveAemetPre():
    with app.app_context():
        db = mongo.db.aemetPrediccion
        db2 = mongo2.db.aemetPrediccion

        humidity = []
        dirWinds = []
        velWinds = []
        rain = []
        temperature = []
        pressure = []
        clouds = []
        hours = []

        DateFind = False
        tam = 0

        obj = json.loads(dumps(db.find({}).sort("_id",-1)))#obtengo el ultimo
        _id=obj[0]['_id']        

        dateHour = datetime.strptime(_id, '%d-%m-%y %H')#Quitar :%M:%S

        formatoDate = "%d-%m-%Y"
        date = dateHour.strftime(formatoDate)
        obj_dateDatetime = datetime.strptime(date,formatoDate)

        formatoHour = "%H"
        hour = dateHour.strftime(formatoHour)        
        obj_hourDatetime = datetime.strptime(hour,formatoHour)

        #Calculo la fechas en las que tengo que recoger datos
        primeraFecha = obj[0]['obj'][0]['prediccion']['dia'][0]['fecha']
        datetimePrimera = datetime.strptime(primeraFecha, '%Y-%m-%d')
        
        primerPeriodo = obj[0]['obj'][0]['prediccion']['dia'][0]['temperatura'][0]['periodo']
        datetimePrimera = datetimePrimera + timedelta(hours = int(primerPeriodo))

        end_date = datetimePrimera + timedelta(hours = 48)

        #Determinar len for

        for dia in xrange(0,len(obj[0]['obj'][0]['prediccion']['dia'])):
            for hora in xrange(1,len(obj[0]['obj'][0]['prediccion']['dia'][dia]['temperatura'])):
                ultimaFecha = obj[0]['obj'][0]['prediccion']['dia'][dia]['fecha']
                dateultimaFecha = datetime.strptime(ultimaFecha, '%Y-%m-%d')
                ultimoPeriodo = obj[0]['obj'][0]['prediccion']['dia'][dia]['temperatura'][hora]['periodo']
                    #dateultimoPeriodo = datetime.strptime(ultimoPeriodo, '%H')
                dateFechaActual = dateultimaFecha + timedelta(hours = int(ultimoPeriodo))

                if dateFechaActual == end_date:  
                    DateFind = True
                    tam = dia
        
        if DateFind == False:
            end_date = end_date - timedelta(hours = 1)
            for dia in xrange(0,len(obj[0]['obj'][0]['prediccion']['dia'])):
                for hora in xrange(1,len(obj[0]['obj'][0]['prediccion']['dia'][dia]['temperatura'])):
                    ultimaFecha = obj[0]['obj'][0]['prediccion']['dia'][dia]['fecha']
                    dateultimaFecha = datetime.strptime(ultimaFecha, '%Y-%m-%d')
                    ultimoPeriodo = obj[0]['obj'][0]['prediccion']['dia'][dia]['temperatura'][hora]['periodo']
                        #dateultimoPeriodo = datetime.strptime(ultimoPeriodo, '%H')
                    dateFechaActual = dateultimaFecha + timedelta(hours = int(ultimoPeriodo))

                    if dateFechaActual == end_date:  
                        DateFind = True
                        tam = dia

        for dia in xrange(0,tam+1):

            aux = obj[0]['obj'][0]['prediccion']['dia'][dia]['fecha']
            obj_hourstime = datetime.strptime(aux, '%Y-%m-%d')
        
            for hora in xrange(0,len(obj[0]['obj'][0]['prediccion']['dia'][dia]['humedadRelativa'])):
                humidity.append(float(obj[0]['obj'][0]['prediccion']['dia'][dia]['humedadRelativa'][hora]['value']))
                periodo = obj[0]['obj'][0]['prediccion']['dia'][dia]['humedadRelativa'][hora]['periodo']
                end_date = obj_hourstime + timedelta(hours=int(periodo))
                hours.append(end_date)

            for hora in xrange(0,len(obj[0]['obj'][0]['prediccion']['dia'][dia]['vientoAndRachaMax']),2):                
                dirWinds.append(obj[0]['obj'][0]['prediccion']['dia'][dia]['vientoAndRachaMax'][hora]['direccion'][0])
                velWinds.append(float(obj[0]['obj'][0]['prediccion']['dia'][dia]['vientoAndRachaMax'][hora]['velocidad'][0]))

            for hora in xrange(0,len(obj[0]['obj'][0]['prediccion']['dia'][dia]['precipitacion'])):  
                auxRain = obj[0]['obj'][0]['prediccion']['dia'][dia]['precipitacion'][hora]['value']
                if auxRain != "Ip":
                        rain.append(float(auxRain))
                else:
                    rain.append(float(0))  

            for hora in xrange(0,len(obj[0]['obj'][0]['prediccion']['dia'][dia]['temperatura'])): 
                temperature.append(float(obj[0]['obj'][0]['prediccion']['dia'][dia]['temperatura'][hora]['value']))
                pressure.append(None)
                clouds.append(None)

        db2.insert(
                {
                "_id":_id,
                "fecha":obj_dateDatetime,
                "hora":obj_hourDatetime,
                        
                            "horas":[
                                {
                                    "hora":hours[0],
                                    "humedad":{
                                        "valor":humidity[0],
                                        "unidad":"%"
                                    },
                                    "dirViento":{
                                        "valor":dirWinds[0],
                                    },
                                    "velViento":{
                                        "valor":velWinds[0],
                                        "unidad":"km/h"
                                    },
                                    "lluvia":{
                                        "valor":rain[0],
                                        "unidad":"mm"
                                    },
                                    "temperatura":{
                                        "valor":temperature[0],
                                        "unidad":"°C"
                                    },
                                    "presion":{
                                        "valor":pressure[0],
                                        "unidad":"hPa"
                                    },
                                    "nubes":{
                                        "valor":clouds[0],
                                        "unidad":"%"
                                    }
                                }
                            ]
                        
                                                                                
                }
            )

        for x in xrange(1,len(temperature)):
            db2.update(
                    {"_id":_id},
                        { '$push': 
                            {"horas": {
                                "hora":hours[x],
                                "humedad":{
                                    "valor":humidity[x],
                                    "unidad":"%"
                                },
                                "dirViento":{
                                    "valor":dirWinds[x],
                                },
                                "velViento":{
                                    "valor":velWinds[x],
                                    "unidad":"km/h"
                                },
                                "lluvia":{
                                    "valor":rain[x],
                                    "unidad":"mm"
                                },
                                "temperatura":{
                                    "valor":temperature[x],
                                    "unidad":"°C"
                                },
                                "presion":{
                                    "valor":pressure[x],
                                    "unidad":"hPa"
                                },
                                "nubes":{
                                    "valor":clouds[x],
                                    "unidad":"%"
                                }
                            
                            }
                        } 
                    }
                )
        humidity = []
        dirWinds = []
        velWinds = []
        rain = []
        temperature = []
        pressure = []
        clouds = []
        hours = []


        print "Guardado coleccion AemetPrediccion en Historico: ", time.ctime()