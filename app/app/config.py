# -*- coding: utf-8 -*-
from run import app

DEBUG = True

app.config['MONGO_DBNAME'] = 'DB_DatosMeteorologicos'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/DB_DatosMeteorologicos'

app.config['MONGO2_DBNAME'] = 'Historico'
app.config['MONGO2_URI'] = 'mongodb://localhost:27017/Historico'


#Apis Server Jaen
url_tiempo = "http://api.tiempo.com/index.php?api_lang=es&localidad=3593&affiliate_id=fb4yufi34e6a&v=2&h=1"

url_openweathermap = "http://api.openweathermap.org/data/2.5/forecast?id=6358500&lang=es&units=metric&appid=524747f062f3acb4b82767ca81412ee0"

url_wunderground = "http://api.wunderground.com/api/45687ffde2931d7f/hourly10day/lang:SP/q/SP/Jaén.json"

url_aemet = "https://opendata.aemet.es/opendata/api/observacion/convencional/datos/estacion/5270B/?api_key=eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1amFlbmlhQGdtYWlsLmNvbSIsImp0aSI6IjY3ZjkzZDJlLWY0M2MtNGQ0NC1iMjFhLTViMmNhZTc5ODE4OSIsImV4cCI6MTUwMjEzODk2NSwiaXNzIjoiQUVNRVQiLCJpYXQiOjE0OTQzNjI5NjUsInVzZXJJZCI6IjY3ZjkzZDJlLWY0M2MtNGQ0NC1iMjFhLTViMmNhZTc5ODE4OSIsInJvbGUiOiIifQ.LMfYC1hzMg6KLhtqEoENoX1KrVjtRo0CPAKT3hYIvb8"

url_aemetPrediccion = "https://opendata.aemet.es/opendata/api/prediccion/especifica/municipio/horaria/23050/?api_key=eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1amFlbmlhQGdtYWlsLmNvbSIsImp0aSI6IjY3ZjkzZDJlLWY0M2MtNGQ0NC1iMjFhLTViMmNhZTc5ODE4OSIsImV4cCI6MTUwMjEzODk2NSwiaXNzIjoiQUVNRVQiLCJpYXQiOjE0OTQzNjI5NjUsInVzZXJJZCI6IjY3ZjkzZDJlLWY0M2MtNGQ0NC1iMjFhLTViMmNhZTc5ODE4OSIsInJvbGUiOiIifQ.LMfYC1hzMg6KLhtqEoENoX1KrVjtRo0CPAKT3hYIvb8"

url_accuweather = "http://dataservice.accuweather.com/forecasts/v1/hourly/12hour/306731?apikey=QeR8OoKMRMgTbSjKyy5gzLzlX5ht5qzC&language=es-es&details=true&metric=true"



