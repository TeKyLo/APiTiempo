# -*- coding: utf-8 -*-
from flask_apscheduler import APScheduler
import os
from run import app
import time
#import logging #Quitar en server Jaen
import sys
sys.path.insert(0, 'models/')

class ConfigIni(object):
    if os.environ.get("WERKZEUG_RUN_MAIN") == "true":
        JOBS = [
            {
                'id': 'init',
                'func': 'scheduler:init',
               	'run_date': '2017-05-26 00:00:00'
            }

        ]

def init():
    print "-Puesta en marcha servidor", time.ctime()
    app.config.from_object(Config())
    scheduler = APScheduler()
    scheduler.init_app(app)
    scheduler.start()


class Config(object):
    if os.environ.get("WERKZEUG_RUN_MAIN") == "true":
        JOBS = [
            {
                'id': 'llamadaTiempo',
                'func': 'modelsTiempo:llamadaTiempo',
                'trigger': 'interval',
                'hours': 6
            },
            {
                'id': 'saveTiempo',
                'func': 'modelsTiempo:saveTiempo',
                'trigger': 'interval',
                'hours': 8
            },
            {
                'id': 'llamadaOpenweathermap',
                'func': 'modelsOpenweathermap:llamadaOpenweathermap',
                'trigger': 'interval',
                'hours': 6
            },         
            {
                'id': 'saveOpen',
                'func': 'modelsOpenweathermap:saveOpen',
                'trigger': 'interval',
                'hours': 8
            },
            {
                'id': 'llamadaWunderground',
                'func': 'modelsWunderground:llamadaWunderground',
                'trigger': 'interval',
                'hours': 6
            },
            {
                'id': 'saveWunder',
                'func': 'modelsWunderground:saveWunder',
                'trigger': 'interval',
                'hours': 8
            },
            {
                'id': 'llamadaAemet',
                'func': 'modelsAemet:llamadaAemet',
                'trigger': 'interval',
                'hours': 6
            },
            {
                'id': 'saveAemet',
                'func': 'modelsAemet:saveAemet',
                'trigger': 'interval',
                'hours': 8
            },
            {
                'id': 'llamadaAemetPrediccion',
                'func': 'modelsAemetPrediccion:llamadaAemetPrediccion',
                'trigger': 'interval',
                'hours': 6
            },
            {
                'id': 'saveAemetPre',
                'func': 'modelsAemetPrediccion:saveAemetPre',
                'trigger': 'interval',
                'hours': 8
            },
            {
                'id': 'llamadaAccuweather',
                'func': 'modelsAccuweather:llamadaAccuweather',
                'trigger': 'interval',
                'hours': 6
            },
            {
                'id': 'saveAccu',
                'func': 'modelsAccuweather:saveAccu',
                'trigger': 'interval',
                'hours': 8
            },
            {
                'id': 'separator',
                'func': 'scheduler:separator',
                'trigger': 'interval',
                'hours': 10
            }

        ]

def separator():
    print "-----------------------------------"

    
#werkzeug_logger = logging.getLogger('werkzeug')#Quitar en server Jaen
#werkzeug_logger.setLevel(logging.INFO)#Quitar en server Jaen

app.config.from_object(ConfigIni())
schedulerInicio = APScheduler()
schedulerInicio.api_enabled = True
#logging.basicConfig() #Quitar en server Jaen
schedulerInicio.init_app(app)
schedulerInicio.start()
